var express = require('express');
var app = express();
var port = process.env.PORT || 5000;
var flash = require('connect-flash');

var morgan = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var session = require('express-session');

app.use(morgan('dev')); // log requests in console
app.use(cookieParser()); // read cookies
app.use(bodyParser());
app.use( bodyParser.json() );       // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
  extended: true
})); 

app.use(express.static(__dirname + '/app'));

// Allows the .html suffix and renders by using ejs.
app.engine('html', require('ejs').renderFile);
app.set('views', __dirname + '/app');
app.set('view engine', 'ejs'); // set up templates

app.use(flash()); // use connect-flash for flash messages stored in session

app.listen(port);
console.log("Server is up on " + port);

var repo = null;
var exec = require('child_process').exec;

app.get("/", function(req, res){
	console.log("Index");
});

app.get('/commit/:id', function(req, res){
	console.log(req.params.id);
	res.render("commit.html");
});

app.post('/set/repo',  function(req, res){
	repo = req.body.repo;

	res.redirect("/dashboard");
});

app.get("/dashboard", function(req, res){
	res.render("dashboard.html");
});

app.get('/git/log/commits', function(req, res){
	var cmd = 'git --git-dir ' + repo +  ' log --pretty=oneline --author-date-order';

	exec(cmd, function(error, stdout, stderr) {
	  	res.send(stdout.split("\n"));
	});
});

app.get('/git/log/commits/stats', function(req, res){
	var cmd = 'git --git-dir ' + repo + ' shortlog -s -n --all --no-merges';
	exec(cmd, function(error, result, err) {
	  	res.send(result);
	});
});

app.get('/git/diff/:commit', function(req, res){
	var commitID = req.params.commit;

	var cmd = 'git --git-dir ' + repo +  ' show --pretty ' + commitID;

	exec(cmd, function(error, stdout, stderr) {
	  	res.send(stdout);
	});
});