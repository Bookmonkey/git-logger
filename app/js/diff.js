

httpGetAsync('http://localhost:5000/git/diff/9df4806f69eb80ffdde3177ad2cfbab79c9d8eaf', function(data){
	var commit = JSON.stringify(data.replace(/\t/g, " "));

	commit = commit.replace(/</g, "&lt;");
	commit = commit.replace(/>/g, "&gt;");

	var splitter = JSON.parse(commit);

	var array = splitter.split("\n");

	var newArray = [];

	for(var s = 0; s < array.length; s++){
		if(array[s] !== " "){
			newArray.push(array[s]);
		}
	}

	array = newArray;

	console.log(array);
	var newCommitString = "";
	for(var i = 0; i < array.length; i++){
		var tmpString = "";
		var tmpArray = array[i].split(" ");
		console.log(tmpArray);
		
		if (array[i].charAt(0) === "-"){
			tmpString = "<p class='red'>" + array[i] + "</p";
		}
		
		if(tmpArray[0] === "diff" && tmpArray[1] === "--git"){
			tmpString = "<hr><p><strong>" + array[i] + "</strong></p>";
		}
		else if(array[i].charAt(0) === "+"){
			tmpString = "<p class='green'>" + array[i] + "</p>";
		}
		else{
			tmpString = "<p>" + array[i] + "</p>";
		}

		newCommitString += tmpString;
	}

	document.getElementById("diff").innerHTML = newCommitString;


});