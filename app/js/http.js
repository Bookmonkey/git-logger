function httpGetAsync(theUrl, callback)
{
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.onreadystatechange = function() { 
        if (xmlHttp.readyState == 4 && xmlHttp.status == 200)
            callback(xmlHttp.responseText);
    }
    xmlHttp.open("GET", theUrl, true); // true for asynchronous 
    xmlHttp.send(null);
}

function httpPost(url, data, callback){
	// construct an HTTP request
	var xhr = new XMLHttpRequest();
	xhr.open("POST", url, true);

	// console.log(JSON.stringify(data));
	// send the collected data as JSON
	xhr.send({data: data});

	xhr.onloadend = function () {
	// done
	};
}
