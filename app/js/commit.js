httpGetAsync('http://localhost:5000/git/log/commits', function(data){
	var commit = document.getElementById('commit');
	var parsed = JSON.parse(data);

	var numberOfCommits = parsed.length;

	for(var i = 0; i < parsed.length; i++){
		var para = document.createElement('p');
		var commitID = parsed[i].substring(0, 40);

		var link = document.createElement('a');
		link.setAttribute('href', "http://localhost:5000/commit/" + commitID);
		link.appendChild(document.createTextNode(commitID.substring(0, 5)));
		para.appendChild(link);
		para.appendChild(document.createTextNode(parsed[i].substring(40, parsed[i].length)));
		commit.appendChild(para);
	}
});

httpGetAsync("/git/log/commits/stats", function(data){
	var stats = JSON.stringify(data).split("\\n");

	for(var i = 0; i < stats.length; i++){
		stats[i] = stats[i].replace(/\"/g, "").trim();

		var tmpStat = stats[i].split("\\t");
		stats[i] = {
			commits: tmpStat[0],
			author: tmpStat[1],
		};
	}
	console.log(stats);
	// document.getElementById("stats").innerHTML = stats;

	for(var s = 0; s < stats.length; s++){
		var para = document.createElement("p");
		para.appendChild(document.createTextNode(stats[s].commits + " commits by " + stats[s].author));

		document.getElementById("stats").appendChild(para);
	}
});